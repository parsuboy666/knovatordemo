---- NodeReact Application Workflow

- /Client directory contains react code for frontend.

- /nodebackend directory contains nodeserver code for backend.

- .gitlab-ci.yml files have all of the ci-cd script.

For the above deployment I am using an EC2 Instance and GitLab CI-CD Pipelines.

- /LaravelSetup directory Contains text files for the plan setup of Distributed Laravel app
