import React, { useState, useEffect } from 'react';
import { getMessage, getProductList } from './api';
import './App.css';

function App() {
  const [message, setMessage] = useState('');
  const [list, setList] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const result = await getMessage();
      const list  = await getProductList()
      setMessage(result);
      setList(list)
    }
    fetchData();
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>{message}</p>
        <div><ul style={{listStyle:'none', color:'purple'}}>
        {list.map(data => <li style={{paddingBottom:'20px'}}>{data}</li>)}
        </ul></div>
      </header>
      
    </div>
  );
}

export default App;
