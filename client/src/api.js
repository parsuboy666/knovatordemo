const API_URL = process.env.REACT_APP_API_URL || 'http://13.234.18.211:5000/api';

export const getMessage = async () => {
  const response = await fetch(`${API_URL}/message`);
  const data = await response.json();
  return data.message;
};


export const getProductList = async () => {
  const response = await fetch(`${API_URL}/list`);
  const data = await response.json();
  return data.data;
};
