const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 5000;

app.use(cors());


app.get('/api/message', (req, res) => {
  res.json({ message: 'This is for Knovator DevOps Experiment' });
});

app.get('/api/list', (req, res) => {
  res.json({ message: 'ProductList', data: ["React", "Node", "Docker", "AWS", "Terraform", "Kubernetes"] });
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
